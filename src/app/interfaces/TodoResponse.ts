import {TodoStatus} from "../enums/TodoStatus";

export interface TodoResponse {
  id: number,
  user_id: number,
  title: string,
  due_on: Date,
  status: TodoStatus
}
