import { Injectable } from '@angular/core';
import {filter, mergeAll, Observable, skip, take} from "rxjs";
import {TodoResponse} from "./interfaces/TodoResponse";
import {TodoStatus} from "./enums/TodoStatus";
import {TodoRepositoryService} from "./todo-repository.service";

@Injectable()

export class AppService {

  constructor(
    private todoRepositoryService: TodoRepositoryService
  ) { }

  public getSecondTodo(): Observable<TodoResponse>{
    return this.todoRepositoryService.getTodos()
      .pipe(
        mergeAll(),
        filter((todoResponse: TodoResponse) => todoResponse.status === TodoStatus.PENDING),
        skip(1),
        take(1),
      )
  }
}
