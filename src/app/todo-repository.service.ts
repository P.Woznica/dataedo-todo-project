import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {TodoResponse} from "./interfaces/TodoResponse";
import {HttpClient} from "@angular/common/http";

@Injectable()

export class TodoRepositoryService {
  private readonly todoEndpoint = 'https://gorest.co.in/public/v2/todos'

  constructor(
    private httpClient: HttpClient
  ) { }

  public getTodos(): Observable<TodoResponse[]> {
    return this.httpClient.get<TodoResponse[]>(this.todoEndpoint)
  }
}
